---
layout: layout
title: RMSTokenView
---

This is a test post writen in markdown. To learn more about markdown check out the [documentation](http://daringfireball.net/projects/markdown/) on [Daring Fireball](http://daringfireball.net/).


{% highlight objc %}
#import "RMSTokenView.h"
#import "RMSTokenConstraintManager.h"

void *RMSTokenSelectionContext = &RMSTokenSelectionContext;
NSString *RMSBackspaceUnicodeString = @"\u200B";

@interface RMSTokenView()
@property (nonatomic, strong) UIView *content;
@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) UILabel *summaryLabel;

@property (nonatomic, strong) NSArray *contentConstraints;

@property (nonatomic, strong) NSMutableArray *tokenViews;
@property (nonatomic, strong) NSMutableArray *tokenLines;
@property (nonatomic, strong) UIButton *selectedToken;

@property (nonatomic) CGSize lastKnownSize;

@property (nonatomic, strong) RMSTokenConstraintManager *constraintManager;

@end

@implementation RMSTokenView

#pragma mark - Setup

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    if (CGRectIsEmpty(self.frame)) {
        self.frame = CGRectMake(0, 0, 320, 44);
    }
    self.contentSize = self.bounds.size;
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor whiteColor];
    
    _tokenViews = [NSMutableArray array];
    _tokenLines = [NSMutableArray arrayWithObject:[NSMutableArray array]];
    
    _constraintManager = [RMSTokenConstraintManager manager];
    _constraintManager.tokenView = self;
    
    [self setupViews];
}

@end
{% endhighlight %}